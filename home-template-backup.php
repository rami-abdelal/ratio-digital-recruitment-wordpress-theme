<?php

/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main" class="home">
   
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
        <p><?php the_content(__('(more...)')); ?></p>
        
        <hr> <?php endwhile; else: ?>
        
        <p><?php _e('Sorry, index php no posts matched your criteria.'); ?></p><?php endif; ?>
        
    <div class="material full specialisms">
       
        <h1 class="page-title text-gradient">Specialisms</h1>
 
        <?php 
        
        $args = array( 'post_type' => 'specialisms', 'posts_per_page' => 10, order => 'ASC');
        
        $loop = new WP_Query( $args );
        
        echo '<div id="specialism-container">';
        
        while ( $loop->have_posts() ) : $loop->the_post();
        
        echo '<a class="specialism" href="';
        
        the_permalink();
        
        echo '">';
        
        echo '<span class="';
        
        the_field('icon');
        
        echo '"></span>';
        
        the_title();

        
        echo '</a>';
        
        endwhile;
        
        echo '</div>';

        ?>
        
    </div>

        
    </div>
   
</div>

<?php get_footer(); ?>