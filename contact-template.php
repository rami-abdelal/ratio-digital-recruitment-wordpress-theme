<?php

/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main" class="contact">
           
       <header class="title">

            <h1 class="full"><?php the_title(); ?></h1>

       </header>
         
        <iframe
          width="100%"
          height="490"
          frameborder="0" style="border:0"
          src="https://www.google.com/maps/embed/v1/place?q=be+offices+1+olympic+way+Wembley+Middlesex+HA9+0NP&key=AIzaSyCJEdJZ7LCezEeEk_qKg4kVrW5D78qgcg8">
        </iframe>
          
        <div class="dark bar">
            
            <div class="full">
                
                <p>Email us now at <strong>info@ratiodigital.co.uk</strong></p>
                
                <a class="button" href="mailto:info@ratiodigital.co.uk">Email us</a>
                
            </div>
            
        </div>  
        
        <div class="wrap">
            
            <div class="full right">
                
                <h1 class="text-gradient">Our offices</h1>
                
            </div>
            
        </div>
        
        <div class="third-container full">
                        
            <div class="material third">
                <h2 class="text-gradient">London</h2>
                <p>1 Olympic Way<br>
                Wembley<br>
                Middlesex<br>
                HA9 0NP</p>
                <h6>+442076638016</h6>
                <a class="button" href="tel:+442076638016">Call London Office</a>
            </div>
            
            <div class="material third">
                <h2 class="text-gradient">Ireland</h2>
                <p>Ruaine<br>
                Killglass<br>
                Roscommon<br>
                Ireland</p>
                <h6>+353872299905</h6>
                <a class="button" href="tel:+353872299905">Call Ireland Office</a>
            </div>
            
            <div class="material third">
                <h2 class="text-gradient">Australia</h2>
                <p>24 Catherine St<br>
                Coburg North VIC 3058<br>
                Australia</p>
                <h6>+6120415517413</h6>
                <a class="button" href="tel:+6120415517413">Call Australia Office</a>
            </div>
            
        </div>
        
        <div class="dark bar">
            
            <div class="full">
                
                <p>Looking for one of our consultants?</p>
                
                <a class="button" href="<?php echo get_site_url(); ?>/team">
                    Team
                </a>
                
            </div>
            
            
        </div>
                
        <div class="material full form">
            
            <h1 class="text-gradient">Send us a message</h1>
            
            <?php echo do_shortcode('[contact-form-7 id="121" title="Contact Us"]'); ?>
            
        </div>
    
</div>

<?php get_footer(); ?>