<?php 

/*
Template Name: Single specialism
Template Post Type: specialisms
*/

get_header(); ?>

<div id="main">
  
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
   <header class="title">
       
        <h1 class="full"><?php the_title(); ?></h1>
        
   </header>
   
   <div class="half-container full">
      
       <div class="material job half">
          
           <h2 class="text-gradient">Senior Python Developer</h2>
           <table>
               <tr>
                   <td><strong>Location:</strong> London</td>
                   <td><strong>Rate:</strong> up to £50k</td>
               </tr>
               <tr>
                   <td><strong>Date:</strong> 06/06/2017</td>
                   <td><strong>Type:</strong> Permanent</td>
               </tr>
           </table>
           <a href="<?php echo get_site_url(); ?>/jobs" class="button">View details</a>
           
       </div>

       <div class="material job half">
          
           <h2 class="text-gradient">Full Stack Developer</h2>
           <table>
               <tr>
                   <td><strong>Location:</strong> London</td>
                   <td><strong>Rate:</strong> up to £50k</td>
               </tr>
               <tr>
                   <td><strong>Date:</strong> 06/06/2017</td>
                   <td><strong>Type:</strong> Permanent</td>
               </tr>
           </table>
           <a href="<?php echo get_site_url(); ?>/jobs" class="button">View details</a>
           
       </div>
       
   </div>
   
   <div class="find-more-jobs dark bar">
       
       <div class="full">
           
           <p>Find more jobs like these</p>
           
           <a class="button" href="<?php echo get_site_url(); ?>/jobs">Jobs</a>
           
       </div>
       
   </div>
   
   <div id="specialism-content" class="material wide">
       
        <div class="full">

            <h1 class="page-title text-gradient"><?php the_title(); ?></h1>
            
            <?php
                echo '<span class="';

                the_field('icon');

                echo '"></span>';
            ?>

            <?php the_content(__('(more...)')); ?>
            
           <a class="button" href="<?php echo get_site_url(); ?>/jobs">Search for these jobs</a>
            <?php endwhile; else: ?>

            <p><?php _e('Sorry, lol no posts matched your criteria.'); ?></p><?php endif; ?>

        </div>
        
    </div>
    
   <div class="see-more-specialisms dark bar">
       
       <div class="full">
           
           <p>Looking for a different specialism?</p>
           
           <a class="button" href="<?php echo get_site_url(); ?>/specialisms">Specialisms</a>
           
       </div>
       
   </div>
    
</div>

<?php get_footer(); ?>