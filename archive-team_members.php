<?php



/**

 * Template Name: Team Members Archive

 *

 * @package WordPress

 * @subpackage Ratio_Digital_Recruitment

 * @since Ratio Digital Recruitment 1.0

 */





get_header(); ?>



<div id="main">

     

    <header class="title">

       

        <h1 class="full">Team</h1>

        

    </header>



    <div class="third-container team-members full">

       

        <?php 

        

        $args = array( 'post_type' => 'team_members', 'posts_per_page' => 10, order => 'ASC');

        

        $loop = new WP_Query( $args );

    

        while ( $loop->have_posts() ) : $loop->the_post();



        echo '<div id ="';

        

        the_field('team_member_email');

        

        echo '" class="material third">';

        

        echo '<div class="team-image';

        

        $photo = get_field('team_member_photo');

        

        if ($photo) {

            

            echo '" style="background-image: url(';



            the_field('team_member_photo');



            echo ');">';

            

        } else {

            

            

            echo ' new-member">';

            

            echo '<span class="icon-tie"></span>';

            

        }

        

        

        the_title();

        

        echo '</div>';

        

        echo '<p class="team-phone"><strong>Phone: </strong>';

            

        the_field('team_member_phone');

            

        echo '</p>';

        

        echo '<p class="team-specialties"><strong>Specialties: </strong>';

            

        the_field('team_member_specialties');

            

        echo '</p>';

        

        echo '<a class="button" href="mailto:';

    

        echo the_field('team_member_email');

    

        echo '@ratiodigital.co.uk">';

    

        echo 'Get in touch';

        

        echo '</a>';

    

        echo '</div>';

    

        endwhile;

        

        ?>
        

    </div>

       

    <div class="find-more-jobs dark bar">

       

        <div class="full">



            <p>Learn more about us</p>



            <a class="button" href="<?php echo get_site_url(); ?>/about">About</a>



        </div>

       

   </div>



        

</div>





<?php get_footer(); ?>