<?php

/**
 * Template Name: Join Us
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main" class="joinus">
   
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
    <header class="title">

            <h1 class="full"><?php the_title(); ?></h1>

    </header>
          
    <div id="joinus-content" class="material wide">
        
        <div class="full">

            <h1 class="text-gradient">We're hiring</h1>
            
            <?php the_content(__('(more...)')); ?>
            
            <?php endwhile; else: ?>

            <?php endif; ?>
                    
        </div>
        
    </div>
    
    <div class="bar gradient">
        
        <div class="full">
            
            <p>To apply, fill out the form below!</p>
            
        </div>
        
    </div>
       
    <div class="material full">
        
        <h1 class="text-gradient">Apply for a Ratio role</h1>
        
        <?php
        
            echo do_shortcode('[contact-form-7 id="117" title="Join Ratio CV Registration Form"]');
        
        ?>
        
    </div>
        
</div>

<?php get_footer( 'basic' ); ?>