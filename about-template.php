<?php

/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


 get_header(); ?>

<div id="main" class="about">
   

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title">

            <h1 class="full"><?php the_title(); ?></h1>

       </header>
          
    <div class="material wide" id="about-content">
        
        <div class="full">
           
            <?php the_content(__('(more...)')); ?>

            <?php endwhile; else: ?>

            <?php _e('Sorry, index php no posts matched your criteria.'); ?><?php endif; ?>
            
            <a class="button" href="<?php echo get_site_url(); ?>/team">Meet our consultants</a>
            
        </div>
        
    </div>
    
    <div class="bar dark">
       
        <div class="full">
        <p>See our specialisms</p>
        
        <a class="button" href="<?php echo get_site_url(); ?>/specialisms">Specialisms</a>
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>