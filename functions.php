<?php

// Add the filter to manage the p tags
// add_filter( 'the_content', 'wti_remove_autop_for_image', 0 );

function wti_remove_autop_for_image( $content )
{
     global $post;

     
          remove_filter('the_content', 'wpautop');

     return $content;
}

function register_my_menus() {
  register_nav_menus(
    array(
      'side-menu' => __( 'Side Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


function create_post_type() {
    
  register_post_type( 'specialisms',
    array(
      'labels' => array(
        'name' => __( 'Specialisms' ),
        'singular_name' => __( 'Specialism' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'specialisms'),
	  'menu_icon' => 'dashicons-star-filled',
    )
  );
    
  register_post_type( 'team_members',
    array(
      'labels' => array(
        'name' => __( 'Team Members' ),
        'singular_name' => __( 'Team Member' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'team'),
	  'menu_icon' => 'dashicons-universal-access-alt',
    )
  );
    
}

add_action( 'init', 'create_post_type' );
?>