<?php

/**
 * Template Name: Register your CV
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */

get_header(); ?>

<div id="main" class="register-your-cv">
   

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title">

            <h1 class="full"><?php the_title(); ?></h1>

       </header>
          
        <div class="material full">
        <h1 class="text-gradient">Got your CV handy?</h1>
        <h6>Complete the form to register your CV with us!</h6>
        <?php the_content(__('(more...)')); ?>
        
        <?php endwhile; else: ?>
        
        <?php endif; ?>
        
    </div>
    
</div>

<?php get_footer(); ?>