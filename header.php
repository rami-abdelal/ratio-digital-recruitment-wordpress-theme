<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/favicon-512x512.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>

</head>

<body class="closed">
   
    <div id="handle">
    
        <button class="hamburger hamburger--spring" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>

       <span class="title">Ratio Digital Recruitment</span>
       
    </div>
        
    <div id="wrapper">
        <aside>
            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/logo-300x273.png" alt="Ratio Digital Recruitment Logo">
            <?php wp_nav_menu( array( 'theme_location' => 'side-menu' ) ); ?>
        </aside>
    </div>
