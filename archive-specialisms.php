<?php

/**
 * Template Name: Specialisms Archive
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main">
     
    <header class="title">
       
        <h1 class="full">Specialisms</h1>
        
    </header>

    <div class="third-container specialisms full">
       
        <?php 
        
        $args = array( 'post_type' => 'specialisms', 'posts_per_page' => 10, order => 'ASC');
        
        $loop = new WP_Query( $args );
    
        while ( $loop->have_posts() ) : $loop->the_post();

        echo '<div class="material third">';
        
        echo '<div class="specialism-image">';
        
        echo '<span class="';
        
        the_field('icon');
        
        echo '"></span>';
        
        echo '</div>';
        
        echo '<h2 class="text-gradient">';
        
        the_title();

        echo '</h2>';
                                
        echo '<a class="button" href="';
    
        the_permalink();
    
        echo '">';
        
        the_title();
            
        echo '</a>';
    
        echo '</div>';
    
        endwhile;
        
        ?>
                
    </div>
       
    <div class="find-more-jobs dark bar">
       
        <div class="full">

            <p>Search for a job</p>

            <a class="button" href="<?php echo get_site_url(); ?>/jobs">Jobs</a>

        </div>
       
   </div>

        
</div>


<?php get_footer(); ?>