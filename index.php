<?php get_header(); ?>

<div id="main">
   

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title">

            <h1 class="full"><?php the_title(); ?></h1>

       </header>
          
        <div class="material full">
        
        <h1 class="text-gradient"><?php the_title(); ?></h1>
        
        <?php the_content(__('(more...)')); ?>
        
        <?php endwhile; else: ?>
        
        <p><?php _e('Sorry, index php no posts matched your criteria.'); ?></p><?php endif; ?>
        
    </div>
    
</div>

<?php get_footer(); ?>