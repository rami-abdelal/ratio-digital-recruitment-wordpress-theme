<footer>
    <div id="pages-wrap">
        <div id="pages" class="full">
            <h1 class="text-gradient">Pages</h1>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

            <span class="icon-logo"></span>
        </div>
    </div>
    <div class="dark credentials bar">
       
        <div class="full">
            <p>Ratio Digital Recruitment &copy; <?php echo date("Y"); ?>. All Rights Reserved.</p>
        </div>
        
    </div>
</footer>
</div>
</div>
</body>
</html>
