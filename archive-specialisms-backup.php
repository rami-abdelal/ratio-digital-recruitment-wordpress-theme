<?php

/**
 * Template Name: Specialisms Archive
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main">
     
    <header class="title">
       
        <h1 class="full">Specialisms</h1>
        
    </header>

    <div class="material full">

        <?php 
        
        $args = array( 'post_type' => 'specialisms', 'posts_per_page' => 10, order => 'ASC');
        
        $loop = new WP_Query( $args );
        
        echo '<div id="specialism-container">';
        
        while ( $loop->have_posts() ) : $loop->the_post();
        
        echo '<a class="specialism" href="';
        
        the_permalink();
        
        echo '">';
        
        echo '<span class="';
        
        the_field('icon');
        
        echo '"></span>';
        
        the_title();

        
        echo '</a>';
        
        endwhile;
        
        echo '</div>';

        ?>
        
    </div>
    
</div>

<?php get_footer(); ?>