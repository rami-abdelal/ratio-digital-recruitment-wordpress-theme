<footer>
    <div id="refer-wrap">
        <div id="refer" class="full">
            <h1>Refer a friend</h1>
            <h6>And get £250</h6>
            <p>Recommend someone for one of our vacancies here at Ratio Digital Recruitment, or for a vacancy with any other organisation we’re recruiting for to get £250 if they’re hired!</p>
            <a class="button" href="<?php echo get_site_url(); ?>/join-us">Learn more</a>
            <span class="icon-tie"></span>    
        </div>
   </div>
    <div id="pages-wrap">
        <div id="pages" class="full">
            <h1 class="text-gradient">Pages</h1>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

            <span class="icon-logo"></span>
        </div>
    </div>
    <div class="dark credentials bar">
       
        <div class="full">
            <p>Ratio Digital Recruitment &copy; <?php echo date("Y"); ?>. All Rights Reserved.</p>
        </div>
        
    </div>
</footer>
</div>
</div>
</body>
</html>
