<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */

get_header(); ?>

<div id="main" class="home">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <p><?php the_content(__('(more...)')); ?></p>

    <?php endwhile; else: ?>

    <p><?php _e('Sorry, index php no posts matched your criteria.'); ?></p><?php endif; ?>

    <div class="bar gradient">
    
        <div class="full">
            
            <p>Or just give us a ring on <strong>020 7663 8016</strong></p>

            <a class="button" href="tel:02076638016">Call us</a>
            
        </div>
    
    </div>

    <div class="wrap">
        <div class="full">
            <div class="text-gradient">
                <h1>We specialise in</h1>
            </div>
        </div>
    </div>

    <div class="third-container specialisms full">

    <?php 

    $args = array( 'post_type' => 'specialisms', 'posts_per_page' => 3, order => 'ASC');

    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();

    echo '<div class="material third">';
    echo '<div class="specialism-image">';
    echo '<span class="';
    the_field('icon');
    echo '"></span>';
    echo '</div>';
    echo '<h2 class="text-gradient">';
    //the_title();
    echo '</h2>';
    echo '<a class="button" href="';
    the_permalink();
    echo '">';
    the_title();
    echo '</a>';
    echo '</div>';

    endwhile;

    ?>

    </div>   
    
    <div class="bar dark">
    
        <div class="full">
            
            <p>See all of our specialisms</p>

            <a class="button" href="<?php echo get_site_url() ;?>/specialisms/">Specialisms</a>
            
        </div>

    </div>

    <div class="wrap">
        <div class="full">
            <div class="text-gradient">
                <h1>Meet the team</h1>
            </div>
        </div>
    </div>


    <div class="third-container team-members full">

        <?php 

        $args = array( 'post_type' => 'team_members', 'posts_per_page' => 10, order => 'ASC');
        $loop = new WP_Query( $args );

        while ( $loop->have_posts() ) : $loop->the_post();

        echo '<div id ="';
        the_field('team_member_email');
        echo '" class="material third">';
        echo '<div class="team-image';
        $photo = get_field('team_member_photo');
        if ($photo) {
            echo '" style="background-image: url(';
            the_field('team_member_photo');
            echo ');">';
        } else {
            echo ' new-member">';
            echo '<span class="icon-tie"></span>';
        }
        the_title();
        echo '</div>';
        echo '<p class="team-phone"><strong>Phone: </strong>';
        the_field('team_member_phone');
        echo '</p>';
        echo '<p class="team-specialties"><strong>Specialties: </strong>';
        the_field('team_member_specialties');
        echo '</p>';
        echo '<a class="button" href="mailto:';
        echo the_field('team_member_email');
        echo '@ratiodigital.co.uk">';
        echo 'Get in touch';
        echo '</a>';
        echo '</div>';
        endwhile;

        ?>

</div>


    <div class="bar dark">

        <div class="full">
            
            <p>Meet the rest of our team</p>

            <a class="button" href="<?php echo get_site_url() ;?>/team/">Team</a>
            
        </div>

    </div>

</div>



<?php get_footer(); ?>